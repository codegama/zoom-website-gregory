<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'user' , 'middleware' => 'cors'], function() {

    Route::any('pages_list' , 'ApplicationController@static_pages_api');

    Route::get('get_settings_json', function () {

        if(\File::isDirectory(public_path(SETTINGS_JSON))){

        } else {

            \File::makeDirectory(public_path('default-json'), 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(public_path(SETTINGS_JSON));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

    Route::post('home' , 'UserApiController@home');

	/***
	 *
	 * User Account releated routs
	 *
	 */

    Route::post('/register','UserApiController@register');
    
    Route::post('/login','UserApiController@login');

    Route::post('/forgot_password', 'UserApiController@forgot_password');

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('/profile','UserApiController@profile'); // 1

        Route::post('/update_profile', 'UserApiController@update_profile'); // 2

        Route::post('/change_password', 'UserApiController@change_password'); // 3

        Route::post('/delete_account', 'UserApiController@delete_account'); // 4

        Route::post('/push_notification_update', 'UserApiController@push_notification_status_change');  // 5

        Route::post('/email_notification_update', 'UserApiController@email_notification_status_change'); // 6

        Route::post('/logout', 'UserApiController@logout'); // 7

        // CARDS curd Operations

        Route::post('cards_add', 'UserApiController@cards_add'); // 15

        Route::post('cards_list', 'UserApiController@cards_list'); // 16

        Route::post('cards_delete', 'UserApiController@cards_delete'); // 17

        Route::post('cards_default', 'UserApiController@cards_default'); // 18

        Route::post('payment_mode_default', 'UserApiController@payment_mode_default');
        //configurations

    });

    Route::post('/project/configurations', 'UserApiController@configurations'); 

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('subscriptions_index', 'UserApiController@subscriptions_index');

        Route::post('subscriptions_view', 'UserApiController@subscriptions_view');
        
        Route::post('subscriptions_payment_by_card', 'UserApiController@subscriptions_payment_by_card');

        Route::post('subscriptions_payment_by_paypal', 'UserApiController@subscriptions_payment_by_paypal');

        Route::post('subscriptions_history', 'UserApiController@subscriptions_history');

    });

    Route::post('meetings_index', 'UserApiController@meetings_index');

    Route::post('meetings_view', 'UserApiController@meetings_view');

    Route::post('meetings_limit_check', 'UserApiController@meetings_limit_check');

    Route::post('meetings_start', 'UserApiController@meetings_start');
    
    Route::post('meetings_connection_update', 'UserApiController@meetings_connection_update');

    Route::post('meetings_members', 'UserApiController@meetings_members');

    Route::post('meetings_members_join', 'UserApiController@meetings_members_join');

    Route::post('meetings_end', 'UserApiController@meetings_end');

    Route::post('meetings_status', 'UserApiController@meetings_status');

});
