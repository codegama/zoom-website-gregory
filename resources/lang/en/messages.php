<?php

return array(

	/*********** FORM FIELDS ************/

	'submit' => 'Submit',
	'reset' => 'Reset',
	'close' => 'Close',

	'active' => 'Active',
	'inactive' => 'Inactive',
	'activate' => 'Activate',
	'inactivate' => 'In Activate',

	'id'	=>	'ID',
	'email' => 'Email',
	'email_address' => 'Email Address',
	'password' =>  'Password',
	'confirm_password' => 'Confirm Password',
	'new_password' => 'New Password',
	'old_password' => 'Old Password',

	'first_name' => 'First Name',
	'full_name' => 'Full Name',
	'last_name' => 'Last Name',
	'username' => 'User Name',

	'mobile' => 'Mobile',
	'picture' => 'Picture',
	'about' => 'About Me',
	'address' => 'Address',
	'description' => 'Description',
	'name' => 'Name',
	'title' => 'Title',
	'type' => 'Type',
	'gender' =>'Gender',
	'remove' => 'Remove',
	'amount'	=> 	'Amount',
	'currency' 	=>	'Currency',
	'remember_me' => 'Remember Me',
	'other_details' => 'Other Details',
	'date_of_birth' => 'Date Of Birth',
	'male' => 'Male',
	'female' => 'Female',
	'joined_date' => 'Joined Date',

	/*********** FORM FIELDS ************/
	

	/*********** COMMON CONTENT *******/

	'select_picture' => 'Selecte Picture',
	'member_since' => "Since ",
	'user_info' => 'User Info',

	'version' => 'Version',
	'copyright' => 'Copyright',
	'all_right_reserved' => 'All right reserved',
	'analytics' => 'Analytics',
	'core' => 'Core',

	'approve'	=>	'Approve',
	'decline'	=>	'Decline',
	'approved'	=>	'Approved',
	'pending'	=>	'Pending',
	'added'	=> 	'Added',

	'YES' => 'Yes',
	'NO' => 'No',

	'user' => 'User',
	'admin' => 'Admin',
	'user_welcome_title' => 'Welcome to ',
	'login_content' => 'Login to your account',

	'login_type' => 'Login Type',

	'action' => 'Action',

	'edit' => 'Edit',

	'status' => 'Status',

	'joined' => 'Joined At',

	's_no' => 'S NO',

	'delete' => 'Delete',

	'approve' => 'Approve',

	'decline' => 'Decline',

	'approved' => 'Approved',

	'pending' => 'Pending',

	'declined' => 'Declined',

	'verify' => 'Verify',

	'verified' => 'Verified',

	'unverify' => 'Unverify',

	'help' => 'Help',
	'faq' => 'FAQ',

	'settings' => 'Settings',
	'profile' => 'Profile',
	'dashboard' => 'Dashboard',
	'revenue_dashboard' => 'Revenue Dashboard',
	'login' => 'Login',
	'signup' => 'Signup',
	'account' => 'Account',
	'edit_profile' => 'Edit Profile',
	'change_password' => 'Change Password',
	'delete_account' => 'Delete Account',
	'logout' => 'Logout',
	'hello_text' => 'Hello! let\'s get started',

	'device_type' => 'Device Type',
	'login_by' => 'Login By',
	'register_type' => 'Register Type',
	'payment_mode' => 'Payment Mode',
	'timezone' => 'Timezone',
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',
	'push_notification' => 'Mobile Notification',
	'email_notification' => 'Email Notification',
	'is_email_verified' => 'Is Email Verified?',
	'notification_settings'=>'Notification Settings',

	'history' => 'History',

	'home' => 'Home',

	'on' => 'ON',
	'off' => 'OFF',
	'no' => 'NO',
	'yes' => 'YES',

	'created' => 'Created',
	'updated_on' => 'Updated On',

	'created_at' => 'Created Time',
	'updated_at' => 'Updated Time',

	'file' =>'File',

	'update_profile' => 'Update Profile',
	'change_password' => 'Change Password',

	'view' => 'View',
	'view_all' => 'View All',
	'success' => 'Success!!',

	'total_amount' => 'Total Amount',
	'revenues' => 'Revenues',
	'added_on' => 'Added On',

    'admin_not_error'	=>	'Something Went Wrong, Try Again!',

    'paid' => 'Paid',
	'paid_amount' => 'Paid Amount',
	'admin_amount' => 'Admin Amount',
	'provider_amount' => 'Provider Amount',
	'paid_date' => 'Paid At',
	'sub_total' => 'Sub Total',
	'actual_total' => 'Actual Total',
	'coupon_amount' => 'Coupon Discount',
	'tax_price' =>'Tax Price',
	'details'=>'Details',
	'invoice' => 'Invoice',

	'upload' => 'Upload',
	'upload_image' => 'Upload Image',
	'mobile_note' => 'Note : The mobile must be between 6 and 13 digits.',
	'png_image_note' => 'Please enter .png images only.',
	'description' => 'Description',

	'invalid_request_input' => 'Invalid request details',

	/*********** COMMON CONTENT *******/

	/*********** COMMON ERRORS *******/

	'token_expiry' => 'Token Expired',
	'invalid_token' => 'Invalid Token ',
	'invalid_input' => 'Invalid Input',
	'without_id_token_user_accessing_request' => 'The requested action needs login.',

	'are_you_sure' => 'Are you sure?',
	'unknown_error_occured' => 'Unknown error occured',
	'something_went_wrong' => 'Sorry, Something went wrong, while functioning the current request!!',
	'no_result_found' => 'No Result Found',
	
	'invalid_email_address' => 'The email address is invalid!!',

	'stripe_not_configured' => 'The Stripe Payment is not configured Properly!!!',

	'login_success' => 'Successfully loggedin!!',
	'logout_success' => 'Successfully loggedout!!',

	'mail_send_failure' => 'The mail send process is failed!!!',
	'mail_not_configured' => 'The mail configuration failed!!!',
	'mail_sent_success' => 'Mail sent successfully',

	'forgot_password_email_verification_error' => 'The email verification not yet done Please check you inbox.',
	'forgot_password_decline_error' => 'The requested email is disabled by admin.',
	
	'password_not_correct' => 'Sorry, the password is not matched.',
	'password_mismatch' => 'The password doesn\'t match with existing record. Please try again!!',
	'password_change_success' => 'Password changed successfully!!',

	'account_delete_success' => 'Account deleted successfully!!!',

	'user_details_not_found' => 'The selected user not exists.',
	'provider_details_not_found' => 'The selected provider not exists.',

	/*********** COMMON ERRORS *******/


	'admin_login' => 'ADMIN LOGIN',
	'admin_details_not_found' => 'The selected admin details is not found on the system!!!',
	'admin_profile_success' => 'Profile updated successfully!!',
	'admin_password_change_success' => 'Password Changed successfully',
	'admin_password_mismatch' =>  'Password is mismatched',
	'admin_change_password_confirmation' => "This action will logout the admin automatically. Please Remember the new Password.",

	// DASHBOARD

	'total_profit' => 'Total Profit',
	'today_profit' => 'Today\'s Profit',
	'site_analytics' => 'Site Analytics',

	'total_listings' => 'Total Hosts',
	'total_bookings' => 'Total Bookings',

	'recent_users' => 'Recent Users',
	'recent_providers' => 'Recent Providers',

	'hosts_survey' => 'Host Survey',

	'last_10_days_analytics' => 'Recent site views',

	// USERS 
	'users' => 'Users',
	'user' => 'User',
	'view_users' => 'View Users',
	'view_user' => 'View User',
	'add_user'	=>	'Add User',
	'edit_user'	=>	'Edit User',
	'delete_user'	=>	'Delete User',
	
	'user_created_success' => 'The user created successfully!!',
	'user_updated_success' => 'The user updated successfully!!',
	'user_save_failed' => 'The user details updating failed!!',
	'user_not_found' => 'The selected user details not found.',

	'user_deleted_success' => 'The user deleted successfully!!',
	'user_delete_failed' => 'The user deletion failed!!',
	'user_delete_confirmation' => 'Once you\'ve deleted the record , the user (:other_key) will no longer be able to log in to the site or apps. This action cannot be undone.',

	'user_approve_success' => 'The user approved successfully..!!',
	'user_decline_success' => 'The user declined successfully..!!',
	'user_decline_confirmation' => 'Do you want decline this user?',
	'user_status_change_failed' => 'The user status updating failed..!!',

	'user_verify_success' => 'The user email verification completed!',
	'user_unverify_success' => 'The user email verification removed!',
	'user_verify_change_failed' => 'Updating user email verification status failed..!!',

	'user_email_confirmation' => 'Do you want change this user as email verified user?',

	
	// Pages 

	'static_pages' => 'Pages',
	'add_static_page' => 'Add Page',
	'edit_static_page' => 'Edit Page',
	'view_static_page' => 'View Page',
	'view_static_pages' => 'View Pages',
	'delete_static_page' => 'Delete Page',

	'static_page_created_success' => 'The Page created successfully!!',
	'static_page_updated_success' => 'The Page updated successfully!!',
	'static_page_save_failed' => 'The Page details updating failed!!',
	'static_page_not_found' => 'The Page details not found!!',

	'static_page_deleted_success' => 'The Page deleted successfully!!',
	'static_page_delete_failed' => 'The Page deletion failed!!',
	'static_page_delete_confirmation' => 'Once you\'ve deleted the record , the Page (:other_key) will no longer be available. This action cannot be undo.',

	'static_page_approve_success' => 'The Page Approved successfully..!!',
	'static_page_decline_success' => 'The Page Declined successfully..!!',
	'static_page_decline_confirmation' => 'Do you want decline this Page?',
	'static_page_status_change_failed' => 'The Page status updating failed..!!',

	'static_page_already_alert' => 'The selected page is already exists!!!',

	'static_page_type_edit_note' => 'Can\'t edit the page type. Please delete the page create as a new page',

	'select_static_page_type' => 'Select Page Type',
	'static_page_type' => 'Page Type',
	'heading' => 'Heading',

	// Settings 

	'settings_update_success' => 'Settings Updated successfully!!',
	
	'booking_settings' => 'Booking Settings',

	'settings' => 'Settings',
	'site_settings' => 'Site Settings',
	'live_settings' => 'Live Settings',
	'email_settings' => 'Email Settings',
	'payment_settings' => 'Payment Settings',
	'mobile_settings' => 'Mobile Settings',
	'social_settings' => 'Social Settings',
	'social_and_app_settings' => 'Social & App Settings',
	'social_settings'=>'Social Links',
	'other_settings' => 'Others Settings',
	'push_notification_settings' => 'Push Notification Settings',
	'mail_push_settings' => 'Email & Push Settings',
	
	'payment_settings' => 'Payment Settings',
	

	'stripe_settings' => 'Stripe Settings',

	'site_name' => 'Site Name',
	'site_logo' => 'Site Logo',
	'site_icon' => 'Favicon',
	'tag_name' => 'Tag Name',

	'MAIL_DRIVER' => 'MAIL DRIVER',
	'MAIL_HOST' => 'MAIL HOST',
	'MAIL_PORT' => 'MAIL PORT',
	'MAIL_USERNAME' => 'MAIL USERNAME',
	'MAIL_PASSWORD' => 'MAIL PASSWORD',
	'MAIL_ENCRYPTION' => 'MAIL ENCRYPTION',

	'mail_driver_note' => 'Supported - "smtp", "mailgun"',
	'mail_host_note' => 'Ex- "smtp.gmail.com", "smtp.mailgun.org"',
	'mail_port_note' => 'Ex- 587,445',
	'mail_username_note' => 'Ex- "abcd.gmail.com"',
	'mail_password_note' => '',
	'mail_encryption_note' => 'Ex- "tls"',

	'FCM_SERVER_KEY' => 'FCM SERVER KEY',
	'FCM_SENDER_ID' => 'FCM SENDER ID',
	'FCM_PROTOCOL' => 'FCM PROTOCOL',

	'FCM_SERVER_KEY_note' => "FCM_SERVER_KEY",
	'FCM_SENDER_ID_note' => "FCM_SENDER_ID",
	'FCM_PROTOCOL_note' => "FCM_PROTOCOL",


	'stripe_publishable_key' => 'Publishable Key',
	'stripe_secret_key' => 'Secret key',
	'stripe_mode' => 'Mode',
	'live' => 'Live',
	'sandbox' => 'Sandbox',
	'payment' => 'Payment',

	'user_display_name' => 'User display name',
	'provider_display_name' => 'Provider display name',
	'save' => 'Save',
	'cancel' => 'Cancel',
	'all_right_reserved' => 'All right reserved',
	'select_section_type' => 'Select Section type',
	'section_type' => 'Section Type',
	'my_profile' => 'My Profile',
	'documents_info' => 'Documents Info',
	'static_page_info' => 'Static Page Info',
	'social_login' => 'Social Login',
	'frontend_url' => 'Frontend Url',


	'account_management' => 'Account Management',
	'meetings_management' => 'Meetings Management',
	'profile_management' => 'Profile Management',
	'setting_management' => 'Setting Management',

	'view_spin_packages' => 'View Spin Packages',
	'add_spin_package' => 'Add Spin Package',
	'add_lottery' => 'Add Lottery',
	'view_lotteries' => 'View Lotteries',
	'no_lotteries_found' => 'No Lotteries Found',
	'start_date' => 'Start Date',
	'end_date' => 'End Date',
	'max_limit' => 'Max Limit',
	'per_limit' => 'Per Limit',
	'no_of_credits' => 'No Of Credits',
	'no_of_spins' => 'No of Spins',

	'edit_lottery' => 'Edit Lottery',
	'start_time' => 'Start Time',
	'end_time' => 'End Time',

	'fcm_settings' => 'FCM Settings',
  	'user_fcm_server_key' => 'USER FCM SERVER KEY',
  	'provider_fcm_server_key' => 'PROVIDER FCM SERVER KEY',
  	'user_fcm_sender_id' => 'USER FCM SENDER ID',
  	'provider_fcm_sender_id' => 'PROVIDER FCM SENDER ID',

  	'admin_control' => 'Admin Control',
	'is_demo_control_enabled' => 'Is Demo Control Enabled',
	'is_account_email_verification' => 'Is Account email verification enabled',
	'is_email_notification' => 'Is email notification',
	'is_email_configured' => 'Is email configured',
	'is_push_notification' => 'Is push notification',
	'admin_take_count' => 'Admin Take Count',
	'currency_code' => 'Currency Code',

	'demo_admin_email' => 'Demo - Admin Email',
	'demo_admin_password' => 'Demo - Admin Password',

	'demo_user_email' => 'Demo - User Email',
	'demo_user_password' => 'Demo - User Password',

	'copy_right' => 'Copyright',
	'all_rights' => 'All rights are reserved',

	//dashboard messages
	'total_users' => 'Total Users',
	'total_spin_and_lottery_users' => 'Spin And Lottery statastics',
	'time' => 'Time',

	'MAIL_FROM_ADDRESS' => 'From Address',
	'MAIL_FROM_NAME' => 'From Name',
	'MAIL_FROM_ADDRESS_note' => 'Ex- "no-reply@streamzoom.com"',
	'MAIL_FROM_NAME_note' => 'Ex- "streamzoom"',
	'logout_note' => 'Do you want to logout from this session?',
	'confirm_logout' => 'Confirm Logout',
	'no_user_found' => 'No Users Found',
	'settings_key_not_found' => ' - In this key value not found on settings table.',
	'dob' => 'Date Of Birth',
	'edit_spin_package' => 'Edit Spin Package',
	'fcm_details' => 'FCM Details',
	'what_is_this' => 'What Is this',
	'document_notes' => 'Listed here are all the Document Types you have created. If you delete a document type, it will not appear in the App.',
	'why_you_create_users_manually' => 'Why you create users manually?',
	'users_note' => 'Hey you can see all the users, who are visited to your website',
	'pages_note' => 'Here it will display all the static pages like privacy,policy,terms and conditions pages',
	'payment_management' => 'Payment Management',
	'meetings' => 'Meetings',
	'subscriptions' => 'Subscriptions',
	'subscription_payments' => 'Subscription Payments',

	'add_subscription' => 'Add Subscription',
	'edit_subscriptions' => 'Edit Subscription',
	'view_subscriptions' => 'View Subscriptions',

	'no_of_months' => 'No of months',
	
	'no_of_users'=>'No of users',
	'subscribers' => 'Subscribers',

	'admin_subscription_not_found' => 'Subscription details not found',
	'admin_subscription_create_success' => 'Subscription created successfully',
	'admin_subscription_update_success'	=>	'Subscription updated Successfully',
	'admin_subscription_save_error' => 'Sorry!, something went wrong Subscription details could not be saved. Please try again.',

	'admin_subscription_delete_error' => 'Sorry!, something went wrong Subscription details could not be deleted. Please try again.',
	'admin_subscription_delete_success'	=>	'Subscription deleted successfully',

	'admin_subscription_popular_success' => 'The selected subscription marked as popular',
	'admin_subscription_remove_popular_success' => 'The selected subscription unmarked as popular',
	'admin_subscription_populor_status_error' => 'Sorry!, something went wrong Subscription could not be changed either Popular/Unpopular. Please try again.',

	'admin_subscription_approved_success' => 'Subscription Approved Successfully!!.',
	'admin_subscription_declined_success' => 'Subscription Declined Successfully!!.',	
	'admin_subscription_status_save_error' => 'Sorry!, something went wrong Subscription status could not be changed either Approved/Declined. Please try again.',
	'subscription_delete_confirmation' => 'Once you\'ve deleted the record , the Subscription (:other_key) will no longer be able to log in to the site or apps. This action cannot be undone.',
	'subscription_decline_confirmation' => 'Do you want decline this Subscription?',
	'user_not_available' => 'User Not Available',
	'payment_id' => 'Payment ID',
	'plan_amount' => 'Plan Amount',
	'final_amount' => 'Final Amount',
	'expiry_date' => 'Expiry Date',
	'no_subscriptions_found' => 'No Subscriptions Found',
	'no_subscription_payments_found' => 'No Subscription Payments Found',
	'site_url_settings'=>'Site URL Settings',
	'facebook_link' => 'Facebook Link',
	'linkedin_link' => 'LinkedIn Link',
	'twitter_link' => 'Twitter Link',
	'google_plus_link' => 'Google Plus Link',
	'pinterest_link' => 'Pinterest Link',

	'social_settings' => 'Social Settings',
	'fb_settings' => 'FB Settings',
	'twitter_settings' => 'Twitter Settings',
	'google_settings' => 'Google Settings',

	'FB_CLIENT_ID' => 'FB Client Id',
	'FB_CLIENT_SECRET' => 'FB Client Secret',
	'FB_CALL_BACK' => 'FB CallBack',

	'TWITTER_CLIENT_ID' => 'Twitter Client Id',
	'TWITTER_CLIENT_SECRET' => 'Twitter Client Secret',
	'TWITTER_CALL_BACK' => 'Twitter CallBack',

	'GOOGLE_CLIENT_ID' => 'Google Client Id',
	'GOOGLE_CLIENT_SECRET' => 'Google Client Secret',
	'GOOGLE_CALL_BACK' => 'Google CallBack',

	'PAYPAL_ID' => 'Paypal Id',
	'PAYPAL_SECRET' => 'Paypal Secret',
	'PAYPAL_MODE' => 'Paypal Mode',

	'google_analytics' => 'Google Analytics',
	'body_scripts' => 'Body Scripts' ,
	'header_scripts' => 'Header Scripts',

	'no_meetings_found' => 'No Meetings Found',
	'meeting_name' => 'Meeting Name',
	'start_time' => 'Start Time',
	'end_time' => 'End Name',
	'meeting_initiated' => 'Meeting Initiated',
	'meeting_started' => 'Meeting Started',
	'meeting_completed' => 'Meeting Completed',
	'statistics' => 'Statistics',
	'total_meetings' => 'Total Meetings',
	'subscription_amount' => 'Subscription Amount',
	'meeting_not_available' => 'Meeting Not Available',
	'meeting_not_found' => 'Meeting Not Found',
	'meeting_members' => 'Meeting Members',
	'start_time' => 'Start Time',
	'end_time' => 'End Time',
	'end_meeting' => 'End meeting',
	'view_meetings' => 'View Meetings',
	'meeting_approve_success'=>'The Meeting Approved Successfully',
	'meeting_decline_success'=>'The Meeting Declined Successfully',
	'meeting_status_change_failed' => 'The Meeting status updating failed..!!',
	'meeting_decline_confirmation' => 'Do you want decline this meeting?',
	'meeting_deleted_success' => 'The meeting deleted Successfully!',

	'meeting_delete_confirmation' => 'Once you\'ve deleted the meeting (:other_key), It will not be available. This action cannot be undone.',

	'meeting_end_confirmation' => 'Do you want End the meeting? - :other_key ',
	'meeting_end_success' => 'The meeting ended Successfully!',
	'meeting_end_failed' => 'The meeting ended Successfully!',
	'subscribe' => 'Subscribe',

	'unique_id' => 'Unique Id',
	'payment_id' => 'Payment id',
	'amount' => 'Amount',
	'payment_mode' => 'Payment Mode',
	'is_current_subscription' => 'is current subscription',
	'expiry_date' => 'Expiry Date',
	'status' => 'Status',
	'is_cancelled' => 'Is Cancelled',
	'cancel_reason' => 'Cancel Reason',
	'created_at' => 'Created at',
	'action' => 'action',
	'months' => 'months',
	'month' => 'month',
	'choose' => 'Choose',
	'user_subscription_save' => 'User subscription saved successfully!',
	'user_subscription_save_error' => 'Sorry! User subscription not saved',

	'username_password_not_match' => 'Sorry, the username or password you entered do not match. Please try again',

	'user_profile_update_success' => 'Profile updated',

	'user_forgot_email_title' => 'Your New password',

	'forgot_password' => 'Forgot your password?',
	'forgot_password_text' => 'Your receiving this e-mail because you requested a password reset for your account.',
	'your_password' => 'Your password ',
	'reset_password' => 'Reset password',
	'change_password_email_title' => 'Password Change Alert',
	'change_password_text' => 'Your Password has been Changed Recently. Please Check and Confirm. If Not You can contact Admin Team to Change the Password!',
	'visit_website' => 'Visit Website',

	'free_user_call_time_limit' => 'Guest User - Time limit',
	'free_user_call_members_limit' => 'Guest User - Max users per call',

	'users_meetings_clear_success' => 'The ongoing meetings are cleared',
	'meetings_clear' => 'Meetings clear',
	'clear' => 'Clear',
	'password_change_confirmation' => 'After changing the Password user will automatically logout',
	'cleared'=>'Cleared',
	'mode' => 'Mode',
	'plan' => 'Plan',
	'is_current' => 'Is Current',
	'user_subscriptions' => 'User Subscriptions',
	'total_earnings' => 'Total Earnings',
	'recent_meetings' => 'Recent Meetings',
	'subscription_plans'=>'Plans',
	'no_members_found'=>'No Members Found',
	'payment_not_found_error' => 'Payment details not found!',
	'view_subscription_payments'=>'View Subscription Payments',
	'no_of_hours' => 'No of Hours',
	'no_of_user' => 'User',
	'no_of_hr' => 'Hr',
	'no_of_hrs' => 'No of hrs',
	'duration'=>'Duration',
	'not_available'=>'N/A',
	'OPENVIDU_SERVER_URL' => 'Open Vidu Server URL',
	'OPENVIDU_SECRET' => 'Open Vidu Secret',
	'subscription_decline_confirmation' => 'Do you want to decline this subscription?'
);