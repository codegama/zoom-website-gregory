@extends('layouts.admin')

@section('page_header',tr('subscriptions'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_subscriptions')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_subscriptions')}}

            <a class="btn btn-secondary pull-right" href="{{route('admin.subscriptions.create')}}">
                <i class="fa fa-plus"></i> {{tr('add_subscription')}}
            </a>
        </h4>

    </div>

	<div class="card-body">

		<div class="table-responsive">

            @if(count($subscriptions) > 0)

                <table id="dataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>{{ tr('s_no') }}</th>
                            <th>{{ tr('title') }}</th>
                            <th>{{ tr('no_of_months') }}</th>
                            <th>{{ tr('amount') }}</th>
                            <th>{{ tr('status') }}</th>
                            <th>{{ tr('no_of_users') }}</th>
                            <th>{{ tr('no_of_hrs') }}</th>
                            <th>{{ tr('subscribers') }}</th>
                            <th>{{ tr('action') }}</th>
                        </tr>
                    </thead>

                    <tbody>

                    	@foreach($subscriptions as $i => $subscription_details)
                            <tr>
                                <td>{{$i+$subscriptions->firstItem()}}</td>

                                <td>
                                    <a href="{{ route('admin.subscriptions.view' , ['subscription_id' => $subscription_details->id] ) }}">{{ $subscription_details->title }}</a>
                                </td>

                                <td>{{formatted_plan($subscription_details->plan,$subscription_details->plan_type)}}</td>

                                <td>{{formatted_amount($subscription_details->amount)}}</td>

                                <td>
                                    @if($subscription_details->status ==  YES)
                                        <span class="badge badge-success">{{tr('approved')}}
                                        </span>
                                    @else
                                        <span class="badge badge-danger">{{tr('declined')}}</span> 
                                    @endif
                                </td>

                                <td>{{$subscription_details->no_of_users_formatted}}</td>

                                <td>{{$subscription_details->no_of_hrs_formatted}}</td>

                                <td>
                                    <a href="{{ route('admin.subscription.payments' , ['subscription_id' => $subscription_details->id] ) }}">{{ $subscription_details->subscribers_count }}</a>
                                    
                                </td>

                                <td>
                                    
                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{tr('action')}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                          
                                            <a class="dropdown-item" href="{{ route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id]) }}">
                                                {{tr('view')}}
                                            </a>
                                            
                                            @if(Setting::get('is_demo_control_enabled') == NO)

                                                <a class="dropdown-item" href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id]) }}">
                                                    {{tr('edit')}}
                                                </a>
                                                
                                                <a class="dropdown-item" href="{{route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id])}}" 
                                                onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $subscription_details->name)}}&quot;);">
                                                    {{tr('delete')}}
                                                </a>
                                            @else

                                                <a class="dropdown-item" href="javascript:;">{{tr('edit')}}</a>
                                              
                                                <a class="dropdown-item" href="javascript:;">{{tr('delete')}}</a>                           
                                            @endif
                                            

                                            @if($subscription_details->status == APPROVED)

                                                <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
                                                    {{ tr('decline') }} 
                                                </a>

                                            @else
                                                
                                                <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
                                                    {{ tr('approve') }} 
                                                </a>
                                                   
                                            @endif

                                        </div>

                                    </div>

                                </td>
                             
                            </tr>
                        @endforeach
                       
                    </tbody>
                    
                </table>

                <div class="pull-right">{{ $subscriptions->links() }}</div>

            @else

                <h3 class="no-result">{{ tr('no_subscriptions_found') }}</h3>
                
            @endif

        </div>
        
	</div>
	
</div>

@endsection