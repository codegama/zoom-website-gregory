@extends('layouts.admin')

@section('page_header',tr('meetings'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('meetings')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('meetings')}}
        </h4>

    </div>

	<div class="card-body">

		<div class="table-responsive">

            @if(count($meetings) > 0)

                <table id="dataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
							<th>{{tr('username')}}</th>
							<th>{{tr('meeting_name')}}</th>
							<th>{{tr('start_time')}}</th>
							<th>{{tr('end_time')}}</th>
							<th>{{tr('no_of_users')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                    	@foreach($meetings as $i => $meeting_details)
                           
                            <tr>
                           
                                <td>{{$i+$meetings->firstItem()}}</td>

                                <td>
                                    <a href="{{ route('admin.users.view', ['user_id' => $meeting_details->user_id]) }}">{{($meeting_details->user) ? $meeting_details->user->name : tr('user_not_available')}}
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">{{$meeting_details->meeting_name ?? tr('meeting_not_available')}}
                                    </a>
                                </td>

                                <td>{{ common_date($meeting_details->start_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}
                                </td> 

                                <td>
                                     @if($meeting_details->end_time != '00:00:00')
                                     {{ common_date($meeting_details->end_time, Auth::guard('admin')->user()->timezone,'H:i:s') }}
                                     @else
                                     {{ $meeting_details->end_time }}
                                     @endif
                                </td>

                                <td>{{$meeting_details->no_of_users}}</td>

                                <td>
						      		@if($meeting_details->status == MEETING_INITIATED)
									   <span class="badge badge-success">{{tr('meeting_initiated')}}</span>
									@elseif($meeting_details->status == MEETING_STARTED)
									   <span class="badge badge-primary">{{tr('meeting_started')}}</span>
                                    @else
                                       <span class="badge badge-info">{{tr('meeting_completed')}}</span>
									@endif
						      	</td>

                                <td>
                                    
                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{tr('action')}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                          
                                            <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                {{tr('view')}}
                                            </a>
                                            
                                            @if(Setting::get('is_demo_control_enabled') == NO)
                                                
                                                <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id,'page'=>$page])}}" 
                                                onclick="return confirm(&quot;{{tr('meeting_delete_confirmation', $meeting_details->meeting_name)}}&quot;);">
                                                    {{tr('delete')}}
                                                </a>
                                            @else

                                                <a class="dropdown-item" href="javascript:;">{{tr('delete')}}</a>                           
                                            @endif
                                            
                                            @if($meeting_details->status == MEETING_INITIATED || $meeting_details->status == MEETING_STARTED)
                                            <div class="dropdown-divider"></div>

                                                <a class="dropdown-item" href="{{ route('admin.meetings.end', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{tr('meeting_end_confirmation', $meeting_details->meeting_name)}}&quot;);">
                                                {{tr('end_meeting')}}
                                                </a>
                                            @endif
                                        </div>

                                    </div>

                                </td>
                             
                            </tr>
                        @endforeach
                       
                    </tbody>
                    
                </table>

                <div class="pull-right">{{ $meetings->links() }}</div>

            @else

                <h3 class="no-result">{{ tr('no_meetings_found') }}</h3>
                
            @endif

        </div>
        
	</div>
	
</div>

@endsection