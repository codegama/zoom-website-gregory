<footer class="footer mt-auto">
   
    <div class="copyright bg-white">
        <p>
             {{tr('copy_right')}} &copy; {{date('Y')}} <a class="text-primary" href="{{Setting::get('frontend_url')}}" target="_blank">{{Setting::get('site_name' , 'StreamZoom')}}</a>. {{tr('all_rights')}}
        </p>
    </div>
    
</footer>
