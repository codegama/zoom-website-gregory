<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBillingInfo extends Model
{
	/**
     * Get the User that owns the UserCard.
     */
    public function user() {

        return $this->belongsTo(UserBillingInfo::class, 'user_id');
    }
}
