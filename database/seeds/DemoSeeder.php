<?php

use Illuminate\Database\Seeder;

use App\Helpers\Helper;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('admins')) {

            $check_admin_details = DB::table('admins')->where('email' , 'admin@streamzoom.com')->count();

            if(!$check_admin_details) {

            	DB::table('admins')->insert([
            		[
        		        'name' => 'Admin',
        		        'email' => 'admin@streamzoom.com',
        		        'password' => \Hash::make('123456'),
        		        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'status' => 1,
        		        'created_at' => date('Y-m-d H:i:s'),
        		        'updated_at' => date('Y-m-d H:i:s')
        		    ]
                ]);

            }

            $check_test_admin_details = DB::table('admins')->where('email' , 'test@streamzoom.com')->count();

            if(!$check_test_admin_details) {

                DB::table('admins')->insert([

                    [
                        'name' => 'Test',
                        'email' => 'test@streamzoom.com',
                        'password' => \Hash::make('123456'),
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ],
    		    ]);
            }
        
        }

        if(Schema::hasTable('users')) {

            $check_admin_details = DB::table('users')->where('email' , 'admin@streamzoom.com')->count();

            if(!$check_admin_details) {

                DB::table('users')->insert([
                    [
                        'name' => 'user',
                        'email' => 'user@streamzoom.com',
                        'password' => \Hash::make('123456'),
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'login_by' => 'manual',
                        'device_type' => 'web',
                        'status' => USER_APPROVED,
                        'is_verified' => USER_EMAIL_VERIFIED,
                        'token' => Helper::generate_token(),
                        'token_expiry' => Helper::generate_token_expiry(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

            }

            $check_test_admin_details = DB::table('users')->where('email' , 'test@streamzoom.com')->count();

            if(!$check_test_admin_details) {

                DB::table('users')->insert([

                    [
                        'name' => 'Test',
                        'email' => 'test@streamzoom.com',
                        'password' => \Hash::make('123456'),
                        'picture' => envfile('APP_URL')."/placeholder.jpg",
                        'login_by' => 'manual',
                        'device_type' => 'web',
                        'status' => USER_APPROVED,
                        'is_verified' => USER_EMAIL_VERIFIED,
                        'token' => Helper::generate_token(),
                        'token_expiry' => Helper::generate_token_expiry(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ],
                ]);
            }
        
        }
    }
}
